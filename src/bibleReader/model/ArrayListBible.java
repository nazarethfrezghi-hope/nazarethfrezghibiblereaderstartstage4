package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface). Modified February 9, 2015.
 * @editedBy Nazareth Frezghi, 2019
 */
public class ArrayListBible implements Bible {

	// ArrayListBible Fields.

	ArrayList<Verse> verses; // An ArrayList of verses.

	String version; // the version.

	String title; // the title of this version of the Bible.

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses
	 *            All of the verses of this version of the Bible.
	 */
	public ArrayListBible(VerseList verses) {
        this.verses = verses.copyVerses();
		
		version = verses.getVersion();
		
		title = verses.getDescription();
	}

	/**
	 * Returns the number of verses that this Bible is currently storing.
	 * 
	 * @return the number of verses in the Bible.
	 */
	public int getNumberOfVerses() {
		return verses.size();
	}

	/**
	 * Returns which version this object is storing (e.g. ESV, KJV)
	 * 
	 * @return A string representation of the version.
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * Returns the title of this version of the Bible.
	 * 
	 * @return A string representation of the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Returns whether or not the reference is actually in the Bible
	 * 
	 * @param ref
	 *            the reference to look up
	 * @return true if and only if ref is actually in this Bible
	 */
	public boolean isValid(Reference ref) {
		boolean temp = false;
		for (Verse var : verses) {
			if (var.getReference().getBook().equals(ref.getBook())
					&& var.getReference().getChapter() == ref.getChapter()
					&& var.getReference().getVerse() == ref.getVerse()) {
				temp = true;
			}
		}
		return temp;
	}

	/**
	 * Return the text of the verse with the given reference
	 * 
	 * @param ref
	 *            the Reference to the desired verse.
	 * @return A string representation of the text of the verse or null if the
	 *         Reference is invalid.
	 */
	public String getVerseText(Reference ref) {
		if (isValid(ref)) {
			for (Verse var : verses) {
				if (var.getReference().getBook().equals(ref.getBook())
						&& var.getReference().getChapter() == ref.getChapter()
						&& var.getReference().getVerse() == ref.getVerse()) {
					return var.getText();
				}
			}
		}
		return null;
	}

	/**
	 * Return a Verse object for the corresponding Reference.
	 * 
	 * @param ref
	 *            A reference to the desired verse.
	 * @return A Verse object which has Reference r, or null if the Reference is
	 *         invalid.
	 */
	public Verse getVerse(Reference ref) {
		if (isValid(ref)) {
			for (Verse var : verses) {
				if (var.getReference().getBook().equals(ref.getBook())
						&& var.getReference().getChapter() == ref.getChapter()
						&& var.getReference().getVerse() == ref.getVerse()) {
					return var;
				}
			}

		}
		return null;
	}

	/**
	 * @param book
	 *            the book of the Bible
	 * @param chapter
	 *            the chapter of the book
	 * @param verse
	 *            the verse within the chapter
	 * @return the verse object with reference "book chapter:verse", or null if
	 *         the reference is invalid.
	 */
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		if (isValid(ref)) {
			for (Verse var : verses) {
				if (var.getReference().getBook().equals(ref.getBook())
						&& var.getReference().getChapter() == ref.getChapter()
						&& var.getReference().getVerse() == ref.getVerse()) {
					return var;
				}
			}

		}
		return null;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	// See the Bible interface for the documentation of these methods.
	// Do not over think these methods. All three should be pretty
	// straightforward to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they
	// work better.
	// At that stage you will create another class to facilitate searching and
	// use it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	/**
	 * @return a VerseList containing all of the verses from the Bible, in
	 *         order. The version of the VerseList should be set to the version
	 *         of this Bible and the description should be set to the title of
	 *         this Bible.
	 */
	
	public VerseList getAllVerses() {
		return new VerseList(getVersion(),getTitle(), verses);
		}

	/**
	 * Returns a VerseList of all verses containing <i>phrase</i>, which may be
	 * a word, sentence, or whatever. This method just does simple string
	 * matching, so if <i>phrase</i> is <i>eaten</i>, verses with <i>beaten</i>
	 * will be included.
	 * 
	 * @param phrase
	 *            the word/phrase to search for.
	 * @return a VerseList of all verses containing <i>phrase</i>, which may be
	 *         a word, sentence, or whatever. If there are no such verses,
	 *         returns an empty VerseList. In all cases, the version will be set
	 *         to the version of the Bible (via getVersion()) and the
	 *         description will be set to parameter <i>phrase</i>.
	 */
	public VerseList getVersesContaining(String phrase) {
		// TODO Implement me: Stage 4
		return null;
	}

	/**
	 * Returns a ArrayList<Reference> of all references for verses containing
	 * <i>phrase</i>, which may be a word, sentence, or whatever. This method
	 * just does simple string matching, so if <i>phrase</i> is <i>eaten</i>,
	 * verses with <i>beaten</i> will be included.
	 * 
	 * @param phrase
	 *            the phrase to search for
	 * @return a ArrayList<Reference> of all references for verses containing
	 *         <i>phrase</i>, which may be a word, sentence, or whatever. If
	 *         there are no such verses, returns an empty ArrayList<Reference>.
	 */
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		// TODO Implement me: Stage 4
		return null;
	}

	/**
	 * @param references
	 *            a ArrayList<Reference> of references for which verses are being requested
	 * @return a VerseList with each element being the Verse with that Reference
	 *         from this Bible, or null if the particular Reference does not
	 *         occur in this Bible. Thus, the size of the returned list will be
	 *         the same as the size of the references parameter, with the items
	 *         from each corresponding. The version will be set to the version
	 *         of the Bible (via getVersion()) and the description will be set
	 *         "Arbitrary list of Verses".
	 */
//	public VerseList getVerses(ArrayList<Reference> references) {
//		VerseList verseList = new VerseList(getVersion(),"Arbitrary list of Verses");
//		for (Verse var : verses){
//			for (Reference r : references){
//				if(r.equals(var.getReference())){
//					verseList.add(var);
//				}
//			}
//		}
//		return verseList;
//	}
	public VerseList getVerses(ArrayList<Reference> references) {
		VerseList verseList = new VerseList(getVersion(),"Arbitrary list of Verses");
		for (Reference r : references){
			for (Verse var : verses){
				if(var.getReference().equals(r)){
					verseList.add(var);
				}
			}
		}
		return verseList;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 7.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented
	// by looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	// @Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return -1;
	}

	// @Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 7
		return -1;
	}

	// @Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse,
			Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse,
			Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book,
			int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book,
			int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book,
			int chapter, int verse1, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book,
			int chapter1, int verse1, int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getVersesInclusive(Reference firstVerse,
			Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getVersesExclusive(Reference firstVerse,
			Reference lastVerse) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getBook(BookOfBible book) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1,
			int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}

	// @Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1,
			int chapter2, int verse2) {
		// TODO Implement me: Stage 7
		return null;
	}
}
